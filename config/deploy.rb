require 'mina/bundler'
require 'mina/rails'
require 'mina/git'
require 'mina/newrelic'
# require 'mina/puma'
require "json"

# require 'mina/rbenv'  # for rbenv support. (http://rbenv.org)
# require 'mina/rvm'    # for rvm support. (http://rvm.io)

# Basic settings:
#   domain       - The hostname to SSH to.
#   deploy_to    - Path to deploy into.
#   repository   - Git repo to clone from. (needed by mina/git)
#   branch       - Branch name to deploy. (needed by mina/git)

repo_username = ''
repo_password = ''
begin
  config = JSON.parse(File.read("gitCredentials.json"))
  repo_username = config["username"]
  repo_password = config["password"]
rescue Exception=>e
  STDERR.puts "Failed to read json file at gitCredentials.json. Now you have to login"
end

set :user, 'photostrana'
set :domain, '178.62.253.20'
set :port, 443
set :deploy_to, '/home/photostrana/deploy/photostrana'
set :app_path, lambda { "#{deploy_to}/#{current_path}" }
set :term_mode, :system
if repo_username == ''
  set :repository, "https://codenameisone@bitbucket.org/codenameisone/sokolov.git"
else
  set :repository, "https://#{repo_username}:#{repo_password}@bitbucket.org/codenameisone/sokolov.git"
end
set :branch, 'master'
set :forward_agent, true
set :stage, 'production'
set :rail_env, 'production'
set :rails_env, 'production'

set :newrelic_appname, 'PhotoStrana'
set :newrelic_description, 'http://happy.sokolov.ru/'

set :shared_paths, ['config/application.yml', 'config/database.yml', 'config/secrets.yml', 'log', 'public/uploads', 'tmp/pids', 'tmp/sockets']

task :setup => :environment do

  queue! %(mkdir -p "#{deploy_to}/#{shared_path}/tmp/sockets")
  queue! %(chmod g+rx,u+rwx "#{deploy_to}/#{shared_path}/tmp/sockets")

  queue! %(mkdir -p "#{deploy_to}/#{shared_path}/tmp/pids")
  queue! %(chmod g+rx,u+rwx "#{deploy_to}/#{shared_path}/tmp/pids")

  queue! %[mkdir -p "#{deploy_to}/#{shared_path}/log"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/#{shared_path}/log"]

  queue! %[mkdir -p "#{deploy_to}/#{shared_path}/config"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/#{shared_path}/config"]

  queue! %[touch "#{deploy_to}/#{shared_path}/config/database.yml"]
  queue  %[echo "-----> Be sure to edit '#{deploy_to}/#{shared_path}/config/database.yml'."]

  queue! %[touch "#{deploy_to}/#{shared_path}/config/application.yml"]
  queue  %[echo "-----> Be sure to edit '#{deploy_to}/#{shared_path}/config/application.yml'."]

  queue! %[touch "#{deploy_to}/#{shared_path}/config/secret.yml"]
  queue  %[echo "-----> Be sure to edit '#{deploy_to}/#{shared_path}/config/secret.yml'."]

  queue! %[mkdir -p "#{deploy_to}/shared/public/uploads"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/public/uploads"]
end

namespace :puma do
  desc "Start the application"
  task :start do
    queue 'echo "-----> Start Puma"'
    queue "cd #{app_path} && RAILS_ENV=#{stage} && bin/puma.sh start"
  end

  desc "Stop the application"
  task :stop do
    queue 'echo "-----> Stop Puma"'
    queue "cd #{app_path} && RAILS_ENV=#{stage} && bin/puma.sh stop"
  end

  desc "Restart the application"
  task :restart do
    queue 'echo "-----> Restart Puma"'
    queue "cd #{app_path} && RAILS_ENV=#{stage} && bin/puma.sh restart"
  end
end


desc "Deploys the current version to the server."
task :deploy => :environment do
  deploy do
    # Put things that will set up an empty directory into a fully set-up
    # instance of your project.
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    invoke :'rails:db_migrate'
    invoke :'rails:assets_precompile'
    invoke :'deploy:cleanup'

    to :launch do
      queue "mkdir -p #{deploy_to}/#{current_path}/tmp/"
      queue "rm -rf #{deploy_to}/#{current_path}/public/uploads"
      queue "ln -s #{deploy_to}/#{shared_path}/public/uploads #{deploy_to}/#{current_path}/public"
      invoke :'puma:stop'
      invoke :'puma:start'
      invoke 'newrelic:notice_deployment'
    end
  end
end

# For help in making your deploy script, see the Mina documentation:
#
#  - http://nadarei.co/mina
#  - http://nadarei.co/mina/tasks
#  - http://nadarei.co/mina/settings
#  - http://nadarei.co/mina/helpers
