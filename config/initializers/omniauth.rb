Rails.application.config.middleware.use OmniAuth::Builder do
  provider :odnoklassniki, Figaro.env.odnoklassniki_id, Figaro.env.odnoklassniki_secret, :public_key => Figaro.env.odnoklassniki_public, :scope => 'VALUABLE_ACCESS'
  provider :facebook, Figaro.env.facebook_id, Figaro.env.facebook_secret, :scope => 'email', :locale => 'ru_RU'
  provider :vkontakte, Figaro.env.vkontakte_id, Figaro.env.vkontakte_secret, :scope => 'email'
  provider :developer unless Rails.env.production?
  OmniAuth.config.on_failure = UsersController.action(:new)
end
