Rails.application.routes.draw do

  devise_for :users, :controllers => { omniauth_callbacks: 'callbacks' }
  match '/users/:id/finish_signup' => 'users#finish_signup', via: [:get, :patch], :as => :finish_signup
  match '/users/new' => 'users#new', via: [:get, :post], :as => 'new_user'
  post '/users/create' => 'users#create', :as => 'create_user'

  # Hardcode ok.ru callback route, fixes routing error OAuth
  get '/users/auth/odnoklassniki/callback' => 'callbacks#odnoklassniki'

  devise_for :admins

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'front#index'
  get '/rules' => 'front#rules', :as => 'rules'
  get '/about' => 'front#about', :as => 'about'
  get '/winners' => 'front#winners', :as => 'winners'
  resources :photos, :only => [:create, :new, :show]
  resources :uploads, :only => [:create]
  get '/user/:id' => 'front#user', :as => 'user'
  get '/map(/:alias)' => 'front#map', :as => 'map'


  post '/api/check_email' => 'api#check_email'
  post '/api/create_user' => 'api#create_user'
  post '/api/login_user' => 'api#login_user'
  post '/api/get_points' => 'api#get_points'
  post '/api/get_points_paged' => 'api#get_points_paged'
  post '/api/get_user_points' => 'api#get_user_points'
  post '/api/get_point' => 'api#get_point'
  post '/api/upvote' => 'api#upvote'
  post '/api/load_more_winners' => 'api#load_more_winners'
  post '/api/feedback' => 'api#feedback'

  %w( 404 422 500 ).each do |code|
    get code, :to => 'errors#show', :code => code
  end


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  namespace :admin do
    resources :maps, :only => ['index', 'destroy'] do
      post 'make_primary', on: :member
      post 'unmake_primary', on: :member
      post 'recreate', on: :collection
    end
    resources :users, :only => ['index', 'destroy', 'show']
    resources :photos, :only => ['index', 'destroy', 'show'] do
      post 'make_tile', on: :member
      post 'unmake_tile', on: :member
      post 'publish', on: :member
      post 'unpublish', on: :member
      post 'cancel', on: :member
      get 'unpublished', on: :collection
      get 'cancelled', on: :collection
    end
    resources :regions, :only => ['index', 'show'] do
      resources :generics, :only => ['show', 'create', 'new', 'destroy']
      resources :photos, :only => ['index', 'show', 'destroy'], :controller => 'region_photos' do
        post 'make_tile', on: :member
        post 'unmake_tile', on: :member
        post 'publish', on: :member
        post 'unpublish', on: :member
        post 'cancel', on: :member
      end
    end
    resources :feedbacks, :only => ['index', 'show', 'destroy']
    resources :winners, only: ['index', 'new', 'create']
    resources :prizes, only: ['index', 'new', 'create']
    get "/search" => "dashboard#search"
    get "/" => "dashboard#index"
  end

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
