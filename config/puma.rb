#!/usr/bin/env puma

environment ENV['RAILS_ENV'] || 'production'

daemonize true

pidfile "/home/photostrana/deploy/photostrana/shared/tmp/pids/puma.pid"
stdout_redirect "/home/photostrana/deploy/photostrana/shared/tmp/log/stdout", "/home/photostrana/deploy/photostrana/shared/tmp/log/stderr"

threads 0, 16

bind "unix:///home/photostrana/deploy/photostrana/shared/tmp/sockets/puma.sock"
