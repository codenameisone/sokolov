class WinnerPicker
  attr_reader :week, :ids, :photos
  def initialize(week)
    @week = week
  end

  def pick
    return unless ids.first
    Photo.find(ids.sample)
  end

  private

  def photos
    @photos ||= week.photos.published.where.not(id: excluded_photos_ids).includes(:user)
  end

  def ids
    @ids ||= [].tap do |ids|
      photos.each do |photo|
        weight(photo).times { ids << photo.id }
      end
    end.compact
  end

  def weight(photo)
    return 1 if photo.cached_votes_up < 1
    photo.cached_votes_up
  end

  def excluded_photos_ids
    Photo.published.joins(:winner).where(week_id: week).pluck('photos.id')
  end
end