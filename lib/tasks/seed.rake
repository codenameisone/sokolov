namespace :seed do

  def get_random_record(model)
    offset = rand(model.count)
    model.offset(offset).first
  end

  def create_random_winner(week)
    prize = get_random_record(Prize)
    photo = get_random_record(Photo)
    user = photo.user
    Winner.create!(user: user, prize: prize, photo: photo, week: week)
  end

  desc "Fabricate winners"
  task :winners => :environment do
    current_week = Week.current
    passed_week = Week.where.not(id: current_week.id).first

    # Current week winners
    20.times do
      create_random_winner(current_week)
    end

    # Passed week winners
    24.times do
      create_random_winner(passed_week)
    end
  end

  desc "Fabricate prizes"
  task :prizes => :environment do
    Prize.create!(title: 'Отличный приз')
    Prize.create!(title: 'Хороший приз')
  end
end
