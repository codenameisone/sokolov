namespace :photos do

  desc "Recreate tiles"
  task :recreate_tiles => :environment do

  end

  desc "Create map"
  task :map => :environment do
    Photo.createMap
  end

end
