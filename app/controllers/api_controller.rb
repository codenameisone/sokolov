class ApiController < ApplicationController

  def check_email
    email = params[:email]
    if User.where(:email => email).exists?
      render :json => { :success => :false }
    else
      render :json => { :success => :true }
    end
  end

  def create_user
    user_data = params[:user]
    user = User.new(
      first_name:            user_data[:first_name],
      middle_name:           user_data[:middle_name],
      last_name:             user_data[:last_name],
      post_index:            user_data[:post_index],
      city:                  user_data[:city],
      street:                user_data[:street],
      street_number:         user_data[:street_number],
      house_type:            user_data[:house_type],
      apts_number:           user_data[:apts_number],
      email:                 user_data[:email],
      password:              user_data[:password],
      password_confirmation: user_data[:password]
    )
    if user.save
      sign_in user
      render :json => { :success => :true }
    else
      render :json => { :success => :false }
    end
  end

  def login_user
    user_data = params[:user]
    result = false
    if User.where(:email => user_data[:email]).exists?
      user = User.where(:email => user_data[:email]).first
      if user.valid_password?(user_data[:password])
        result = true
        if user_data[:remember_me] == 'true'
          user.remember_me = true
        end
        sign_in user
      end
    end
    if result
      render :json => { :success => :true }
    else
      render :json => { :success => :false }
    end
  end

  def get_points
    bounds = params[:bounds].split(';')
    @photos = Photo.where('lat <= ?', bounds[2].to_f).where('lat >= ?', bounds[0].to_f).where('lng <= ?', bounds[3].to_f).where('lng >= ?', bounds[1].to_f).published.order('created_at DESC').all
  end

  def get_points_paged
    bounds = params[:bounds].split(';')
    @photos = Photo.where('lat <= ?', bounds[2].to_f).where('lat >= ?', bounds[0].to_f).where('lng <= ?', bounds[3].to_f).where('lng >= ?', bounds[1].to_f).published.order('created_at DESC').page params[:page]
  end

  def get_user_points
    @photos = Photo.where(:user_id => params[:user_id]).published.all
  end

  def upvote
    @photo = Photo.find(params[:id])
    if @photo.present?
      if user_signed_in?
        @user = current_user
        @photo.liked_by @user
        render :json => { :success => :true, :votes => @photo.votes_for.size }
      else
        render :json => { :success => :false, :message => 'you have to log in' }
      end
    else
      render :json => { :success => :false, :message => 'can\'t find photo with this id' }
    end
  end

  def load_more_winners
    @winners = Winner.former.order('created_at desc').offset(params[:offset]).limit(12)
    if @winners.present?
      render partial: 'common/winners', layout: false, locals: { winners: @winners }
    else
      render nothing: true
    end
  end

  def feedback
    feedback_data = params[:feedback]
    feedback = Feedback.new(
      first_name: feedback_data[:first_name],
      last_name: feedback_data[:last_name],
      email: feedback_data[:email],
      message: feedback_data[:message]
    )
    if feedback.save
      render :json => { :success => :true }
    else
      render :json => { :success => :false }
    end
  end

end
