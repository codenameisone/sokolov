class ErrorsController < ApplicationController

  def show
    render status_code.to_s, :status => status_code, :layout => layout
  end

  protected

  def status_code
    params[:code] || 500
  end

  def layout
    !%w(422 500).include?(params[:code])
  end

end
