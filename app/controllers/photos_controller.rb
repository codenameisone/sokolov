class PhotosController < ApplicationController

  before_filter :ensure_signup_complete, only: [:create, :new]
  before_filter :redirect_to_root, only: [:new, :create]

  def show
    @photo = Photo.includes([:region, :user]).find(params[:id])
    if user_signed_in?
      if current_user.voted_for? @photo
        @can_vote = false
      else
        @can_vote = true
      end
    else
      @can_vote = false
    end
  end

  def new
    @photo = Photo.new
  end

  def create
    @photo = Photo.new(photo_params)
    @photo.user_id = current_user.id

    region_title = params[:photo][:region_name]
    if region_title == 'Центральный федеральный округ'
      region_title = 'Московская область'
    end
    if region_title == 'Северо-Западный федеральный округ'
      region_title = 'Ленинградская область'
    end
    if region_title == 'Севастополь'
      region_title = 'Республика Крым'
    end

    region = Region.where('title like ?',region_title).first
    @photo.region_id = region.id
    @photo.week_id = Week.current.id
    @photo.image = File.open(File.join(Rails.root, "public/"+params[:photo][:image_url]))
    if @photo.save
      UserMailer.uploaded_email(@photo).deliver_now
    end
  end

  private

  def photo_params
    params.require(:photo).permit([:title, :lat, :lng])
  end

  def redirect_to_root
    redirect_to root_path
  end

end
