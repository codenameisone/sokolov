class UploadsController < ApplicationController

  skip_before_action :verify_authenticity_token

  def create
    if user_signed_in?
      @upload = Upload.new(upload_params)
      @upload.user_id = current_user.id
      if @upload.save
        render :json => { :success => :true, :url => @upload.image_url(:thumb), :marker => @upload.image_url(:marker) }
      else
        render :json => { :success => :false, :message => @upload.errors[:image].join(' ') }
      end
    else
      render :json => { :success => :false, :message => 'You have to log in first' }
    end
  end

  private
  def upload_params
    params.require(:upload).permit(:image)
  end

end
