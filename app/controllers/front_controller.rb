class FrontController < ApplicationController

  def index
    @map = Map.order('created_at DESC').first
  end

  def rules

  end

  def about

  end

  def winners
    @current_winners = Winner.current
    @current_winners = Winner.previous_week unless @current_winners.present?
    @former_winners  = Winner.former.order('created_at desc').limit(12)
  end

  def new_photo
    @photo = Photo.new
  end

  def photo
    @photo = Photo.includes([:region, :user]).find(params[:id])
    if user_signed_in?
      if current_user.voted_for? @photo
        @can_vote = false
      else
        @can_vote = true
      end
    else
      @can_vote = false
    end
  end

  def map
    if params[:alias].present?
      @region = Region.where(:alias_path => params[:alias]).first
      @points = Photo.where(:region_id => @region.id).published
    else
      @points = Photo.published.order('created_at DESC')
    end
  end

  def user
    @user = User.find(params[:id])
    @photos = Photo.where(:user_id => @user.id).published.order('created_at DESC')
  end

end
