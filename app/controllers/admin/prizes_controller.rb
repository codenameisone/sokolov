class Admin::PrizesController < ApplicationController

  before_action :authenticate_admin!
  layout "admin"

  def new
    @prize = Prize.new
  end

  def index
    @prizes = Prize.order('created_at DESC').page params[:page]
  end

  def create
    @prize = Prize.new(prize_params)
    if @prize.save
      redirect_to admin_prizes_path, notice: 'Приз создан'
    else
      render :action => 'new', :alert => 'Ошибка при создании'
    end
  end

  private

  def prize_params
    params.require(:prize).permit([:title])
  end

end