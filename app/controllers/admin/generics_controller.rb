class Admin::GenericsController < ApplicationController

  before_action :authenticate_admin!

  layout "admin"

  def new
    @region = Region.find(params[:region_id])
    @generic = Generic.new
  end

  def create
    @region = Region.find(params[:region_id])
    @generic = Generic.create(generic_params)
    @generic.lat = @region.lat
    @generic.lng = @region.lng
    if @generic.save
      redirect_to admin_region_path(@region.id), :notice => 'Заглушка создана'
    else
      render :action => 'new', :alert => 'Ошибка при создании'
    end
  end

  def destroy
    @region = Region.find(params[:region_id])
    @generic = Generic.find(params[:id])
    if @generic.destroy
      redirect_to admin_region_path(@region.id), :notice => 'Заглушка удалена'
    else
      redirect_to admin_region_path(@region.id), :notice => 'Ошибка при удалении'
    end
  end

  private

  def generic_params
    params.require(:generic).permit('image', 'region_id')
  end

end
