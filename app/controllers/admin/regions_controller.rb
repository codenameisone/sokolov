class Admin::RegionsController < ApplicationController

  before_action :authenticate_admin!
  layout "admin"

  def index
    @regions = Region.order('title ASC').all
  end

  def show
    @region = Region.find(params[:id])
    @generics = Generic.where(:region_id => @region.id)
    @photos = Photo.where(:region_id => @region.id).order('created_at DESC').page params[:page]
  end

end
