class Admin::MapsController < ApplicationController

  before_action :authenticate_admin!

  layout "admin"

  def index
    @counter = 0
    @error = false
    Region.all.each do |region|
      if Photo.where(:region_id => region.id).where(:is_tile => true).exists?

      else
        if Generic.where(:region_id => region.id).exists?
          
        else
          @error = true
          @counter = @counter + 1
        end
      end
    end
    @map = Map.order('created_at DESC').first
  end

  def unmake_primary
    @map = Map.find(params[:id])
    @map.use_this = false
    @map.save
    flash[:notice] = 'Принудительный показ карты выключен'
    redirect_to admin_maps_path
  end

  def make_primary
    @map = Map.find(params[:id])
    @map.use_this = true
    @map.save
    flash[:notice] = 'Включен принудительный показ карты'
    redirect_to admin_maps_path
  end

  def recreate
    Photo.createMap
    flash[:notice] = 'Карта создана'
    redirect_to admin_maps_path
  end

  def destroy
    Map.destroy(params[:id])
    flash[:notice] = 'Карта удалена'
    redirect_to admin_maps_path
  end

end
