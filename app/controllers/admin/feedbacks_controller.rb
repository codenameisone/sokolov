class Admin::FeedbacksController < ApplicationController

  before_action :authenticate_admin!

  layout "admin"

  def index
    @feedbacks = Feedback.order('created_at DESC').page params[:page]
  end

  def show
    @feedback = Feedback.find(params[:feedback])
  end

  def destroy
    Feedback.destroy(params[:id])
    flash[:notice] = 'Запрос удален'
    redirect_to admin_feedbacks_path
  end

end
