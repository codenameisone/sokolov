class Admin::PhotosController < ApplicationController

  before_action :authenticate_admin!

  layout "admin"

  def index
    @photos = Photo.order('created_at DESC').page params[:page]
  end

  def show
    @photo = Photo.includes([:user, :region]).where(:id => params[:id]).first
  end

  def publish
    @photo = Photo.includes(:user).find(params[:id])
    @photo.is_approved = true
    @photo.save
    UserMailer.approved_email(@photo).deliver_now
    flash[:notice] = 'Фото опубликовано'
    redirect_to :back
  end

  def cancel
    @photo = Photo.includes(:user).find(params[:id])
    @photo.is_cancelled = true
    @photo.save
    UserMailer.cancelled_email(@photo).deliver_now
    flash[:notice] = 'Фото отменено'
    redirect_to :back
  end

  def unpublish
    @photo = Photo.find(params[:id])
    @photo.is_approved = false
    @photo.save
    flash[:notice] = 'Фото снято с публикации'
    redirect_to :back
  end

  def make_tile
    @photo = Photo.find(params[:id])
    @photo.is_tile = true
    @photo.save
    flash[:notice] = 'Фото сделано тайлом для своего региона'
    redirect_to :back
  end

  def unmake_tile
    @photo = Photo.find(params[:id])
    @photo.is_tile = false
    @photo.save
    flash[:notice] = 'Фото убрано из тайлов'
    redirect_to :back
  end

  def unpublished
    @photos = Photo.where(:is_approved => false).where(:is_cancelled => false).order('created_at DESC').page params[:page]
  end

  def cancelled
    @photos = Photo.where(:is_cancelled => true).order('created_at DESC').page params[:page]
  end

end
