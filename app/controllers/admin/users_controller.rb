class Admin::UsersController < ApplicationController

  before_action :authenticate_admin!

  layout "admin"

  def index
    @users = User.order('created_at DESC').page params[:page]
  end

  def show
    @user = User.includes(:photos).where(:id => params[:id]).first
  end

  def destroy

  end

end
