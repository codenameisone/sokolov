class Admin::RegionPhotosController < ApplicationController

  before_action :authenticate_admin!

  layout "admin"

  def index
    @region = Region.find(params[:region_id])
    if params[:sort] == 'popular'
      @photos = Photo.where(:region_id => @region.id).order('created_at DESC').page params[:page]
    else
      @photos = Photo.where(:region_id => @region.id).order('cached_votes_up DESC, created_at DESC').page params[:page]
    end
  end

  def show
    @region = Region.find(params[:region_id])
    @photo = Photo.includes([:user, :region]).where(:id => params[:id]).first
  end

  def publish
    @region = Region.find(params[:region_id])
    @photo = Photo.includes(:user).find(params[:id])
    @photo.is_approved = true
    @photo.save
    UserMailer.approved_email(@photo).deliver_now
    flash[:notice] = 'Фото опубликовано'
    redirect_to :back
  end

  def cancel
    @region = Region.find(params[:region_id])
    @photo = Photo.includes(:user).find(params[:id])
    @photo.is_cancelled = true
    @photo.save
    UserMailer.cancelled_email(@photo).deliver_now
    flash[:notice] = 'Фото отменено'
    redirect_to :back
  end

  def unpublish
    @region = Region.find(params[:region_id])
    @photo = Photo.find(params[:id])
    @photo.is_approved = false
    @photo.save
    flash[:notice] = 'Фото снято с публикации'
    redirect_to :back
  end

  def make_tile
    @region = Region.find(params[:region_id])
    Photo.where(:region_id => @region.id).where(:is_tile => true).each do |photo|
      photo.update_attribute(:is_tile, false)
    end
    @photo = Photo.find(params[:id])
    @photo.is_tile = true
    @photo.save
    flash[:notice] = 'Фото сделано тайлом для своего региона'
    redirect_to :back
  end

  def unmake_tile
    @region = Region.find(params[:region_id])
    @photo = Photo.find(params[:id])
    @photo.is_tile = false
    @photo.save
    flash[:notice] = 'Фото убрано из тайлов'
    redirect_to :back
  end

end
