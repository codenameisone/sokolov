class Admin::WinnersController < ApplicationController

  before_action :authenticate_admin!

  layout "admin"

  def new
    @photo = WinnerPicker.new(Week.previous).pick
    @winner = Winner.new
    @prizes = Prize.all.map { |p| [p.title, p.id] }
  end

  def index
    @winners = Winner.order('created_at DESC').includes(:user, :prize, :photo).page params[:page]
  end

  def create
    @winner = Winner.new(winner_params.merge(additional_params))
    if @winner.save
      redirect_to admin_winners_path, :notice => 'Победитель выбран'
    else
      redirect_to new_admin_winner_path, :alert => 'Ошибка при выборе победителя'
    end
  end

  private

  def winner_params
    params.require(:winner).permit([:photo_id, :prize_id, :user_id])
  end

  def additional_params
    { week_id: Week.current.id }
  end
end