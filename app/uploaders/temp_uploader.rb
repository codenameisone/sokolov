# encoding: utf-8

class TempUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  include CarrierWave::RMagick
  # include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :thumb do
    # process :resize_to_fill => [180, 180]
    # process optimize: [{ quality: 67, quiet: true }]
  end

  version :marker do
    process :resize_to_fill => [36, 36]
    process :convert => "png"
    def full_filename(for_file = model.marker.file)
     "marker.png"
   end
    process :create_marker
  end

  def create_marker

    mask = ::Magick::Image.read(File.join(Rails.root, "data/marker-mask.png")).first
    marker = ::Magick::Image.read(File.join(Rails.root, "data/marker-empty.png")).first

    manipulate! do |img|
      img.composite!(mask, Magick::CenterGravity, Magick::CopyOpacityCompositeOp)
      marker.composite!(img, 2, 2, Magick::OverCompositeOp)
      img = marker
      img = yield(img) if block_given?
      img
    end

  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  # version :thumb do
  #   process :resize_to_fit => [50, 50]
  # end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
    %w(jpg jpeg gif png)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  # def filename
  #   "something.jpg" if original_filename
  # end

end
