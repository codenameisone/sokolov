$ ->
  window.app = $.extend(true, window.app or {},
    init: ->
      $.each @module, (index, module) ->
        if module.init
          module.init.apply this
          module.init = false
        return

      return

    module:
      carousel:
        init: ->

          $('.slides2').each ->
            $(@).slick
              slide: '.slide'
              dots: true

          $window = $(window)
          $scrollableBox = $('.scrollable-box')
          jsp = $scrollableBox.data('jsp')

          if ( $window.width() > 900 )
            $scrollableBox.jScrollPane({hideFocus: true})

          $map = $('#mapview')
          $mapWrap = $map.parent()

          $window.on 'resize', (e) ->
            if (jsp) 
              jsp.reinitialise()


          if ( $('#geo_code_address').length )
            $container = $('#geo_code_address').closest('.collapse')

            $window.on 'resize', (e) ->
              if ( $window.width() < 900 ) 
                jsp = $scrollableBox.data('jsp')
                if ( jsp )
                  jsp.destroy()
                  $scrollableBox = $('.scrollable-box')

                $map.insertAfter( $container )
              else
                jsp = $scrollableBox.data('jsp')
                if ( !jsp )
                  $scrollableBox.jScrollPane({hideFocus: true})
                  jsp = $scrollableBox.data('jsp')

                $map.appendTo( $mapWrap )

          $window.trigger('resize')
          return
  )
  app.init()
  return
