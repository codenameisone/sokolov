$ ->
  window.app = $.extend(true, window.app or {},
    init: ->
      $.each @module, (index, module) ->
        if module.init
          module.init.apply this
          module.init = false
        return

      return

    module:
      map:
        m: ''
        clusterer: ''
        map: ''
        empty_marker: ''
        scroll: ''
        map_marker: ''
        all_loaded: false
        markerOffset: []
        markerPosition: []
        page: 1,
        photo_template: _.template('
          <div class="photo-block" data-id="<%= photo.id %>">
            <div class="image-area">
              <a href="/photos/<%= photo.id %>">
                <img src="<%= photo.path %>">
                <div class="overlay"></div>
              </a>
            </div>
            <div class="description">
              <a class="name" href="/user/<%= photo.user_id %>"><%= photo.name %></a>
              <div class="votes">
                <% if (photo.can_vote == "true") { %>
                  <a class="can-vote js-vote" href="#" data-id="<%= photo.id %>">
                    <span class="icon-like"></span>
                    <span class="icon-like-hover"></span>
                  </a>
                <% } else { %>
                  <% if (photo.voted == "true") { %>
                    <a class="voted js-show-popup" href="#" data-id="<%= photo.id %>" title="Вы уже проголосовали" data-description="За каждую фотографию можно голосовать всего один раз">
                      <span class="icon-like"></span>
                      <span class="icon-like-hover"></span>
                    </a>
                  <% } else { %>
                    <a href="#" class="cant-vote js-social" data-id="<%= photo.id %>" title="Необходимо авторизоваться" data-description="Для того, чтобы иметь возможность голосовать, необходимо войти или зарегистрироваться">
                      <span class="icon-like"></span>
                      <span class="icon-like-hover"></span>
                    </a>
                  <% } %>
                <% } %>
                <span class="counter"><%= photo.votes %></span>
              </div>
            </div>
          </div>
        ')
        load_user_points: (user_id) ->
          self = this
          payload =
            user_id: user_id
          NProgress.start()
          $.post '/api/get_user_points.json', payload, (data) ->
            self.map.geoObjects.removeAll()
            c_points = []
            self.load_photos(data)
            for point in data.points
              pt = new (ymaps.Placemark)([
                point.lat
                point.lng
              ], {pointId: point.id},
                iconLayout: 'default#image'
                iconImageHref: point.icon
                iconImageSize: [
                  40
                  45
                ]
                iconImageOffset: [
                  -20
                  -45
                ])
              pt.events.add 'click', (event) ->
                id = pt.properties.get('pointId')
                location.href = '/photos/'+id;
                return false

              c_points.push pt
              # maps.geoObjects.add pt

            clusterer = new (ymaps.Clusterer)(
              hasBaloon: false
              hasHint: false
              clusterIcons: [ {
                href: self.empty_marker
                size: [
                  40
                  45
                ]
                offset: [
                  -20
                  -45
                ]
              } ]
              clusterNumbers: [ 100 ]
              clusterIconContentLayout: 'cluster#iconContent')

            self.clusterer = clusterer

            self.clusterer.add c_points
            self.map.geoObjects.add self.clusterer
            self.map.setBounds(self.map.geoObjects.getBounds(), { checkZoomRange: true })
            NProgress.done()

        geo_code_address: ->
          self = this
          value = $('#_fake_address').val()
          if value.length > 3
            NProgress.start()
            geo = self.m.geocode('Россия, '+value)
            geo.then ((res) ->
              self.map.setCenter(res.geoObjects.get(0).geometry.getCoordinates(), 10)
              bounds =  res.geoObjects.get(0).properties.get('boundedBy')
              self.map.setBounds(bounds, { checkZoomRange: true })
              NProgress.done()
              return
            ), (err) ->
              NProgress.done()
              return

        dragging: ->
          self = this
          self.marker = $('#marker_dot')
          self.dragger = new ymaps.util.Dragger({autoStartElement: self.marker[0]})
          self.map.geoObjects.removeAll()
          self.dragger.events.add('start', (event) ->

            offset = self.marker.position()
            position = event.get('position')

            self.markerOffset = [
              position[0] - (offset.left)
              position[1] - (offset.top)
            ]
            self.markerPosition = [
              position[0] - (self.markerOffset[0])
              position[1] - (self.markerOffset[1])
            ]
            applyMarkerPosition()
            return
          )

          self.dragger.events.add('move', (event) ->
            applyDelta event
            return
          )

          self.dragger.events.add('stop', (event) ->
            applyDelta event
            self.markerPosition[0] += self.markerOffset[0]
            self.markerPosition[1] += self.markerOffset[1]

            pinPosition = [
              self.markerPosition[0]
              self.markerPosition[0]
            ]

            markerGlobalPosition = self.map.converter.pageToGlobal(self.markerPosition)
            mapGlobalPixelCenter = self.map.getGlobalPixelCenter()
            mapContainerSize = self.map.container.getSize()
            mapContainerHalfSize = [
              mapContainerSize[0] / 2
              mapContainerSize[1] / 2
            ]
            mapGlobalPixelBounds = [
              [
                mapGlobalPixelCenter[0] - (mapContainerHalfSize[0])
                mapGlobalPixelCenter[1] - (mapContainerHalfSize[1])
              ]
              [
                mapGlobalPixelCenter[0] + mapContainerHalfSize[0]
                mapGlobalPixelCenter[1] + mapContainerHalfSize[1]
              ]
            ]

            if containsPoint(mapGlobalPixelBounds, markerGlobalPosition)
              geoPosition = self.map.options.get('projection').fromGlobalPixels(markerGlobalPosition, self.map.getZoom())
              geo = self.m.geocode(geoPosition)
              NProgress.start()
              geo.then ((res) ->
                if res.geoObjects.get(0).properties.get('metaDataProperty.GeocoderMetaData.AddressDetails.Country.CountryName')
                  if res.geoObjects.get(0).properties.get('metaDataProperty.GeocoderMetaData.AddressDetails.Country.CountryName') == 'Россия'

                    address = res.geoObjects.get(0).properties.get('metaDataProperty.GeocoderMetaData.AddressDetails.Country.AddressLine')
                    region = res.geoObjects.get(0).properties.get('metaDataProperty.GeocoderMetaData.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName')

                    if address
                      $('#_fake_address').val(address)
                    if region
                      $('#photo_region_name').val(region)

                    $('#photo_lat').val(geoPosition[0])
                    $('#photo_lng').val(geoPosition[1])

                    pt = new (self.m.Placemark)([
                      geoPosition[0]
                      geoPosition[1]
                    ], {},
                      iconLayout: 'default#image'
                      iconImageHref: window.app.module.upload.marker
                      iconImageSize: [
                        40
                        45
                      ]
                      iconImageOffset: [
                        -20
                        -45
                      ])

                    self.map.geoObjects.add pt
                    self.dragger.destroy()
                    self.marker.css('left', self.marker.data('left'))
                    self.marker.css('top', self.marker.data('top'))
                    self.marker.empty()
                  else

                    self.dragger.destroy()
                    self.marker.css('left', self.marker.data('left'))
                    self.marker.css('top', self.marker.data('top'))
                    self.dragging()

                else
                  self.dragger.destroy()
                  self.marker.css('left', self.marker.data('left'))
                  self.marker.css('top', self.marker.data('top'))
                  self.dragging()

                NProgress.done()
                return
              ), (err) ->
                self.dragger.destroy()
                self.marker.css('left', self.marker.data('left'))
                self.marker.css('top', self.marker.data('top'))
                self.dragging()
                NProgress.done()
                return


            return
          )

          applyDelta = (event) ->
            delta = event.get('delta')
            self.markerPosition[0] += delta[0]
            self.markerPosition[1] += delta[1]
            applyMarkerPosition()
            return

          applyMarkerPosition = ->
            self.marker.css
              left: self.markerPosition[0]
              top: self.markerPosition[1]
            return

          containsPoint = (bounds, point) ->
            point[0] >= bounds[0][0] and point[0] <= bounds[1][0] and point[1] >= bounds[0][1] and point[1] <= bounds[1][1]


        load_point: (id) ->
          self = this
          o = $('#mapview')
          pt = new (ymaps.Placemark)([
            o.data('lat')
            o.data('lng')
          ], {},
            iconLayout: 'default#image'
            iconImageHref: o.data('icon')
            iconImageSize: [
              40
              45
            ]
            iconImageOffset: [
              -20
              -45
            ])

          self.map.geoObjects.add pt

        load_photos: (data) ->
          self = this
          content = ''
          counter = 0
          for photo in data.points
            if counter < 16
              content = content + self.photo_template({photo: photo})
            counter = counter + 1
          $('#photos_area_content').html(content)
          if !self.scroll.length
            self.scroll = $('#photos_area').jScrollPane(
              {
                autoReinitialise: true,
                autoReinitialiseDelay: 100,
                verticalDragMinHeight: 60,
                verticalDragMaxHeight: 60,
                verticalGutter: 0,
                hideFocus: true
              }
            )
            self.scroller = $('#photos_area').data('jsp')
            self.all_loaded = false
            self.page = 1
            self.scroll.bind 'jsp-scroll-y', (event, scrollPositionY, isAtTop, isAtBottom) ->
              if isAtBottom && self.all_loaded == false
                self.page = self.page + 1
                NProgress.start()
                bounds = self.map.getBounds()
                payload =
                  bounds: bounds[0].join(';')+';'+bounds[1].join(';')
                  page: self.page
                $.post '/api/get_points_paged.json', payload, (data) ->
                  if data.points.length > 0
                    content = ''
                    for photo in data.points
                      content = content + self.photo_template({photo: photo})
                    $('#photos_area_content').append(content)
                  else
                    self.all_loaded = true

                  NProgress.done()

          else
            try
              self.scroller.scrollToY(0, false)
            catch e
              return true

            self.all_loaded = false
            self.page = 1

        add_click_event: (point) ->
          point.events.add 'click', (event) ->
            id = point.properties.get('pointId')

            location.href = '/photos/'+id;
            return false

        load_points: (bounds) ->
          self = this
          payload =
            bounds: bounds[0].join(';')+';'+bounds[1].join(';')
          NProgress.start()
          $.post '/api/get_points.json', payload, (data) ->
            self.map.geoObjects.removeAll()
            c_points = []
            self.load_photos(data)

            for point in data.points
              pt = new (ymaps.Placemark)([
                point.lat
                point.lng
              ], {pointId: point.id},
                iconLayout: 'default#image'
                iconImageHref: point.icon
                iconImageSize: [
                  40
                  45
                ]
                iconImageOffset: [
                  -20
                  -45
                ])

              self.add_click_event(pt)

              c_points.push pt
              # maps.geoObjects.add pt

            clusterer = new (ymaps.Clusterer)(
              hasBaloon: false
              hasHint: false
              clusterIcons: [ {
                href: self.empty_marker
                size: [
                  40
                  45
                ]
                offset: [
                  -20
                  -45
                ]
              } ]
              clusterNumbers: [ 100 ]
              clusterIconContentLayout: 'cluster#iconContent')

            self.clusterer = clusterer

            self.clusterer.add c_points
            self.map.geoObjects.add self.clusterer
            NProgress.done()


        load: (ymaps) ->
          self = this

          if $('#mapview').length
            m = $('#mapview')
            console.log m.data('lat')
            console.log m.data('lng')
            self.empty_marker = m.data('empty')
            if m.hasClass('one-photo')
              map = new ymaps.Map 'mapview',
                center: [m.data('lat'), m.data('lng')]
                zoom: m.data('zoom')
                controls: ["zoomControl"]

              self.map = map
              self.load_point(m.data('id'))

            else if m.hasClass('new-photo')

              map = new ymaps.Map 'mapview',
                center: [m.data('lat'), m.data('lng')]
                zoom: m.data('zoom')
                controls: ["zoomControl"]

              self.map = map

              self.map.events.add 'click', (e) ->
                coords = e.get('coords')
                if self.dragger

                  geo = self.m.geocode(coords)

                  NProgress.start()
                  geo.then ((res) ->

                    if res.geoObjects.get(0).properties.get('metaDataProperty.GeocoderMetaData.AddressDetails.Country.CountryName')
                      if res.geoObjects.get(0).properties.get('metaDataProperty.GeocoderMetaData.AddressDetails.Country.CountryName') == 'Россия'

                        address = res.geoObjects.get(0).properties.get('metaDataProperty.GeocoderMetaData.AddressDetails.Country.AddressLine')
                        region = res.geoObjects.get(0).properties.get('metaDataProperty.GeocoderMetaData.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName')

                        if address
                          $('#_fake_address').val(address)
                        if region
                          $('#photo_region_name').val(region)

                        $('#photo_lat').val(coords[0])
                        $('#photo_lng').val(coords[1])

                        self.map.geoObjects.removeAll()
                        pt = new (self.m.Placemark)(coords, {},
                          iconLayout: 'default#image'
                          iconImageHref: window.app.module.upload.marker
                          iconImageSize: [
                            40
                            45
                          ]
                          iconImageOffset: [
                            -20
                            -45
                          ])

                        self.map.geoObjects.add pt
                        self.dragger.destroy()
                        self.marker.css('left', self.marker.data('left'))
                        self.marker.css('top', self.marker.data('top'))
                        self.marker.empty()
                        NProgress.done()

                    NProgress.done()
                    return
                  ), (err) ->
                    NProgress.done()
                    return



                return

              $('body').on 'click', '#geo_code_address', (e) ->
                e.preventDefault()
                self.geo_code_address()

              $('body').on 'keypress', '#_fake_address', (e) ->
                if e.keyCode == 13
                  e.preventDefault()
                  if $(@).val().length > 3
                    self.geo_code_address()

              # self.dragging()

            else if m.hasClass('user-photo')
              map = new ymaps.Map 'mapview',
                center: [m.data('lat'), m.data('lng')]
                zoom: m.data('zoom')
                controls: ["zoomControl"]

              self.map = map

              self.load_user_points($('#mapview').data('user'))

            else
              map = new ymaps.Map 'mapview',
                center: [m.data('lat'), m.data('lng')]
                zoom: m.data('zoom')
                controls: ["zoomControl"]

              self.map = map

              map.events.add 'boundschange', (event) ->
                self.load_points(event.get("newBounds"))
                return

              self.load_points(map.getBounds())


          app.module.map.m = ymaps

          return

        init: ->



          return
  )
  app.init()
  return
