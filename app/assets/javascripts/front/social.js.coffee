$ ->
  window.app = $.extend(true, window.app or {},
    init: ->
      $.each @module, (index, module) ->
        if module.init
          module.init.apply this
          module.init = false
        return

      return

    module:
      social:
        template: _.template('
          <div class="shadow"></div>
          <div class="popup popup-social">
            <div class="closer">&times;</div>
            <div class="container">
              <div class="row">
                <h1>авторизуйтесь через любую<br>социальную сеть:</h1>
                <div class="small-12 columns social-links">
                  <a href="/users/auth/facebook" class="fb">
                    <span class="icon-facebook-round"></span>
                  </a>
                  <a href="/users/auth/vkontakte" class="vk">
                    <span class="icon-vk-round"></span>
                  </a>
                  <a href="/users/auth/odnoklassniki" class="ok">
                    <span class="icon-odnoklassniki-round"></span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        ')
        show: ->
          self = this

          $('.shadow').remove()
          $('.popup').remove()

          $('body').append(self.template())

          $('.popup').on 'click', '.closer', ->
            $('.popup').fadeOut 200, ->
              $(@).remove()
              $('.shadow').fadeOut 100, ->
                $(@).remove()

        init: ->
          self = this

          $('body').on 'click', '.js-social', (e) ->
            e.preventDefault()
            self.show()
            ga('send', 'event', 'Registration', 'FirstStep', 'Sokolov', 500)

          $('body').on 'click', '.js-menu-sandwich', (e) ->
            e.preventDefault()
            $('.layout').toggleClass('menu-opened')

          $('body').on 'click', 'nav.main, .user-controls', (e) ->
            $('.layout').removeClass('menu-opened')

          return
  )
  app.init()
  return
