$ ->
  window.app = $.extend(true, window.app or {},
    init: ->
      $.each @module, (index, module) ->
        if module.init
          module.init.apply this
          module.init = false
        return

      return

    module:
      upload:
        dropzone: ''
        marker: ''
        init: ->
          self = this

          Dropzone.autoDiscover = false

          if $('#new_photo').length


            Dropzone.options.dropzone =
              url: '/uploads'
              uploadMultiple: false
              createImageThumbnails: false
              paramName: 'upload[image]'

            self.dropzone = new Dropzone('#dropzone')

            self.dropzone.on 'sending', (file, xhr, data) ->
              $('#error_explanation').html('')
              NProgress.start()

            self.dropzone.on 'success', (file, data) ->
              if data.success == 'true'
                $('#loaded').html("<img src='"+data.url+"'>")
                $('#dropzone .message').html('')
                top = 15
                left = $('#marker_place').width()/2 - 20
                self.marker = data.marker
                $('#marker_dot').html("<img src='"+data.marker+"'>").css('top', top).css('left', left).data('top', top).data('left',left)
                $('#photo_image_url').val(data.url)
                window.app.module.map.dragging()
              else
                $('#error_explanation').html(data.message)
                $('#photo_image_url').val('')

              NProgress.done()


            $('#new_photo').on 'submit', ->
              all_ok = true
              messages = []
              if $('#photo_image_url').val().length == 0
                all_ok = false
                messages.push 'фотография'

              if $('#photo_title').val().length == 0
                all_ok = false
                messages.push 'название фото'

              if $('#photo_lat').val().length == 0
                all_ok = false
                messages.push 'координаты фото'

              if all_ok
                $('button.rounded-submit').attr('disabled', true);
                ga('send', 'event', 'PhotoUpload', 'Photo', 'Sokolov', 5000);
                return true
              else
                $('button.rounded-submit').attr('disabled', false);
                window.app.module.notice.show('Ошибки при заполнении формы', 'Не заполнены следующие поля: '+messages.join(', ')+'. Заполните их и попробуйте снова')
                return false


          return
  )
  app.init()
  return
