$ ->
  window.app = $.extend(true, window.app or {},
    init: ->
      $.each @module, (index, module) ->
        if module.init
          module.init.apply this
          module.init = false
        return

      return

    module:
      foundation:
        init: ->

          $(document).foundation
            abide:
              timeout: 200
              patterns:
                password: /^(.){8,}$/

          return
  )
  app.init()
  return
