$ ->
  window.app = $.extend(true, window.app or {},
    init: ->
      $.each @module, (index, module) ->
        if module.init
          module.init.apply this
          module.init = false
        return

      return

    module:
      login:
        template: _.template('
          <div class="shadow"></div>
          <div class="popup popup-login">
            <div class="closer">&times;</div>
            <div class="container">
              <h1>Вход на сайт</h1>
              <form method="post" id="login_form" data-abide="ajax">
                <div class="row val-me">
                  <div class="small-6 columns input-holder"><input type="text" required class="email e" name="user[email]" id="email" placeholder="Email"><small class="error">Это обязательное поле</small></div>
                </div>
                <div class="row val-me">
                  <div class="small-6 columns input-holder"><input type="password" required class="password e" name="user[password]" id="password" placeholder="Пароль"><small class="error">Это обязательное поле</small></div>
                </div>
                <div class="row">
                  <div class="small-3 columns remember-me">
                    <input id="remember_me" type="checkbox"><label for="remember_me">Запомнить меня</label>
                  </div>
                  <div class="small-3 columns reset-password">
                    <a href="/users/password/new" class="right">Забыли пароль?</a>
                  </div>
                </div>
                <div class="row centered login-actions">
                  <div class="small-6 columns"><button class="submit-button centered" type="submit">Войти</button></div>
                </div>
                <div class="row social-auth">
                  <div class="small-6 columns">
                    <div class="more-line"><span>ИЛИ</span></div>
                  </div>
                </div>
                <div class="row">
                  <div class="small-6 columns social-links">
                    <a href="/users/auth/facebook" class="fb">
                      <span class="icon-facebook-round"></span>
                    </a>
                    <a href="/users/auth/vkontakte" class="vk">
                      <span class="icon-vk-round"></span>
                    </a>
                    <!--a href="/users/auth/odnoklassniki" class="ok">
                      <span class="icon-odnoklassniki-round"></span>
                    </a-->
                  </div>
                </div>
              </form>
            </div>
          </div>
          ')
        show_popup: ->
          self = this
          $('body').append(self.template)
          $(document).foundation('abide', 'reflow')
          $(document).foundation('tooltip', 'reflow')

          $('#login_form').on 'keyup', 'input', ->
            if $(@).val().length
              $(@).removeClass('e')
            else
              $(@).addClass('e')

          $('.popup').on 'click', '.closer', ->
            $('.popup').fadeOut 200, ->
              $(@).remove()
              $('.shadow').fadeOut 100, ->
                $(@).remove()


          $('#login_form').on 'valid.fndtn.abide', ->
            if $('#remember_me').is(":checked")
              payload =
                user:
                  email: $('#email').val(),
                  password: $('#password').val()
                  remember_me: true
            else
              payload =
                user:
                  email: $('#email').val(),
                  password: $('#password').val()
                  remember_me: false

            $.post '/api/login_user', payload, (data) ->
              if data.success == 'true'
                location.reload()
                return true
              else
                $('.val-me').addClass('error')
                $('.val-me input').eq(0).attr('data-invalid','')
                return false
            return false
        init: ->
          self = this
          $('body').on 'click', '.js-login', (e) ->
            e.preventDefault()
            self.show_popup()

          return
  )
  app.init()
  return
