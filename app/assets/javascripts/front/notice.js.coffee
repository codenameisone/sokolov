$ ->
  window.app = $.extend(true, window.app or {},
    init: ->
      $.each @module, (index, module) ->
        if module.init
          module.init.apply this
          module.init = false
        return

      return

    module:
      notice:
        template: _.template('
          <div class="shadow"></div>
          <div class="popup popup-notice">
            <div class="closer">&times;</div>
            <div class="container">
              <h1><%= notice.title %></h1>
              <p><%= notice.description %></p>
            </div>
          </div>
        ')
        show: (title, description) ->
          self = this

          $('.shadow').remove()
          $('.popup').remove()
          notice =
            title: title
            description: description

          $('body').append(self.template({notice: notice}))

          $('.popup').on 'click', '.closer', ->
            $('.popup').fadeOut 200, ->
              $(@).remove()
              $('.shadow').fadeOut 100, ->
                $(@).remove()

        init: ->
          self = this
          
          $('body').on 'click', '.js-show-popup', (e) ->
            e.preventDefault()
            self.show($(@).attr('title'), $(@).data('description'))

          return
  )
  app.init()
  return
