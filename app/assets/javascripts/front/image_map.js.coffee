$ ->
  window.app = $.extend(true, window.app or {},
    init: ->
      $.each @module, (index, module) ->
        if module.init
          module.init.apply this
          module.init = false
        return

      return

    module:
      image_map:
        init: ->

          $('img[usemap]').rwdImageMaps()

          return
  )
  app.init()
  return
