$ ->
  window.app = $.extend(true, window.app or {},
    init: ->
      $.each @module, (index, module) ->
        if module.init
          module.init.apply this
          module.init = false
        return

      return

    module:
      feedback:
        template: _.template('
          <div class="shadow"></div>
          <div class="popup popup-feedback">
            <div class="closer">&times;</div>
            <div class="container">
              <h1>Обратная связь</h1>
              <div class="row"><div class="small-6 columns feed-notice"><p>Вы можете задать нам интересующие вас вопросы по конкурсу или написать предложения, замечания по работе сайта.</p></div></div>
              <form method="post" id="feedback_form" data-abide="ajax">
                <div class="row val-me">
                  <div class="small-3 columns input-holder"><input type="text" required class="last_name e" name="feedback[last_name]" id="last_name" placeholder="Фамилия"><small class="error">Это обязательное поле</small></div>
                  <div class="small-3 columns input-holder"><input type="text" required class="first_name e" name="feedback[first_name]" id="first_name" placeholder="Имя"><small class="error">Это обязательное поле</small></div>
                </div>
                <div class="row val-me">
                  <div class="small-4 columns input-holder" id="email_holder"><input type="email" required id="feedback_email" class="email e" name="feedback[email]" placeholder="E-mail"><small class="error">Неправильный email</small></div>
                  <div class="small-2 columns input-holder">
                    <span data-tooltip aria-haspopup="true" class="rounded-help has-tip" title="Вы получите ответ на этот e-mail адрес">?</span>
                  </div>
                </div>
                <div class="row">
                  <div class="small-6 columns textarea input-holder">
                    <textarea id="message" placeholder="Сообщение" required></textarea>
                    <small class="error">Это обязательное поле</small>
                  </div>
                </div>
                <div class="row actions">
                  <div class="small-6 columns"><button class="right submit-button" type="submit">Отправить</button></div>
                </div>
              </form>
            </div>
          </div>
          ')
        show_popup: ->
          self = this
          $('body').append(self.template)
          if $('#user_data')
            p = $('#user_data')
            $('#first_name').val(p.data('first-name'))
            $('#last_name').val(p.data('last-name'))
            $('#feedback_email').val(p.data('email'))

          $(document).foundation('abide', 'reflow')
          $(document).foundation('tooltip', 'reflow')

          $('#feedback_form').on 'keyup', 'input, textarea', ->
            if $(@).val().length
              $(@).removeClass('e')
            else
              $(@).addClass('e')

          $('.popup').on 'click', '.closer', ->
            $('.popup').fadeOut 200, ->
              $(@).remove()
              $('.shadow').fadeOut 100, ->
                $(@).remove()


          $('#feedback_form').on 'valid.fndtn.abide', ->
            payload =
              feedback:
                first_name: $('#first_name').val(),
                last_name: $('#last_name').val(),
                email: $('#feedback_email').val(),
                message: $('#message').val()

            $.post '/api/feedback.json', payload, (data) ->
              if data.success == 'true'
                window.app.module.notice.show('Ваше сообщение отправлено', 'Мы ответим вам в ближайшее время')
                return false

            return false
        init: ->
          self = this
          $('body').on 'click', '.js-feedback', (e) ->
            e.preventDefault()
            self.show_popup()

          return
  )
  app.init()
  return
