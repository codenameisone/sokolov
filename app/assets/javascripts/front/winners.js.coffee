$ ->
  window.app = $.extend(true, window.app or {},
    init: ->
      $.each @module, (index, module) ->
        if module.init
          module.init.apply this
          module.init = false
        return

      return

    module:
      winner:
        load_more: (el) ->
          self = this
          winners_container = $('.winners-wrap.row')
          offset = el.data('offset')
          payload =
            offset: offset
          NProgress.start()
          $.post '/api/load_more_winners', payload, (data) ->
            if data
              winners_container.append(data)
              el.data('offset', offset + 12)
              el.blur()
            else
              el.remove()

            NProgress.done()

        init: ->
          self = this
          $('body').on 'click', '.js-load-more', (e) ->
            e.preventDefault()
            self.load_more($(@))

          return
  )
  app.init()
  return
