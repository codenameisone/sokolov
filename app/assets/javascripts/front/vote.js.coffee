$ ->
  window.app = $.extend(true, window.app or {},
    init: ->
      $.each @module, (index, module) ->
        if module.init
          module.init.apply this
          module.init = false
        return

      return

    module:
      vote:
        upvote: (el) ->
          self = this
          payload =
            id: el.data('id')
          NProgress.start()
          $.post '/api/upvote', payload, (data) ->
            if data.success == 'true'
              el.removeClass('can-vote').addClass('voted').addClass('js-show-popup')
              el.attr('title', 'Вы уже проголосовали')
              el.data('description', 'За каждую фотографию можно голосовать всего один раз')
              el.next().html(data.votes)

            NProgress.done()

        init: ->
          self = this
          $('body').on 'click', '.js-vote', (e) ->
            e.preventDefault()
            self.upvote($(@))

          return
  )
  app.init()
  return
