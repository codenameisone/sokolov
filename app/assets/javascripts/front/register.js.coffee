$ ->
  window.app = $.extend(true, window.app or {},
    init: ->
      $.each @module, (index, module) ->
        if module.init
          module.init.apply this
          module.init = false
        return

      return

    module:
      register:
        template: _.template('
          <div class="shadow"></div>
          <div class="popup">
            <div class="closer">&times;</div>
            <div class="container">
              <h1>Регистрация на сайте</h1>
              <form method="post" id="register_form" data-abide="ajax">
                <div class="row">
                  <div class="small-3 columns input-holder"><input type="text" required class="last-name e" name="user[last_name]" id="last_name" placeholder="Фамилия"><small class="error">Это обязательное поле</small></div>
                  <div class="small-3 columns input-holder"><input id="first_name" type="text" required class="first-name e" name="user[first_name]" placeholder="Имя"><small class="error">Это обязательное поле</small></div>
                </div>
                <div class="row">
                  <div class="small-3 columns input-holder" id="email_holder"><input type="email" required id="register_email" class="email e" name="user[email]" placeholder="E-mail"><small class="error">Неправильный email или он уже зарегистрирован</small></div>
                  <div class="small-3 columns input-holder">
                    <span data-tooltip aria-haspopup="true" class="rounded-help has-tip" title="Введите ваш e-mail адрес">?</span>
                  </div>
                </div>
                <div class="row">
                  <div class="small-3 columns input-holder"><input type="password" required class="password e" name="user[password]" id="user_password" placeholder="Пароль"><small class="error">Минимальная длина &mdash; 8 символов</small></div>
                  <div class="small-3 columns input-holder"><input type="password" required class="password-confirm e" name="user[password_confirmation]" data-equalto="user_password" placeholder="Еще раз"><small class="error">Пароли должны совпадать</small></div>
                </div>
                <div class="row social-auth">
                  <div class="small-6 columns">
                    <div class="more-line"><span>ИЛИ</span></div>
                  </div>
                </div>
                <div class="row">
                  <div class="small-6 columns social-links">
                    <a href="/users/auth/facebook" class="fb">
                      <span class="icon-facebook-round"></span>
                    </a>
                    <a href="/users/auth/vkontakte" class="vk">
                      <span class="icon-vk-round"></span>
                    </a>
                    <!--a href="/users/auth/odnoklassniki" class="ok">
                      <span class="icon-odnoklassniki-round"></span>
                    </a-->
                  </div>
                </div>
                <div class="row actions">
                  <div class="small-6 columns"><button class="right submit-button" type="submit">Зарегистрироваться</button></div>
                </div>
              </form>
            </div>
          </div>
          ')
        init: ->
          self = this
          $('body').on 'click', '.js-register', (e) ->
            e.preventDefault()
            $('body').append(self.template)
            $(document).foundation('abide', 'reflow')
            $(document).foundation('tooltip', 'reflow')

            $('#register_form').on 'keyup', 'input', ->
              if $(@).val().length
                $(@).removeClass('e')
              else
                $(@).addClass('e')

            $('.popup').on 'click', '.closer', ->
              $('.popup').fadeOut 200, ->
                $(@).remove()
                $('.shadow').fadeOut 100, ->
                  $(@).remove()


            $('#register_form').on 'valid.fndtn.abide', ->
              $('#register_form').addClass('valid')
              payload =
                email: $('#register_email').val()
              $.post '/api/check_email', payload, (data) ->
                if data.success == 'true'
                  payload =
                    user:
                      first_name: $('#first_name').val(),
                      last_name: $('#last_name').val(),
                      email: $('#register_email').val(),
                      password: $('#user_password').val()

                  $.post '/api/create_user', payload, (data) ->
                    if data.success == 'true'
                      location.reload()
                      return true
                    else
                      return false
                  return true
                else
                  # exists
                  holder = $('#email_holder')
                  holder.addClass('error')
                  holder.find('input').eq(0).attr('data-invalid','')
                  return false

              return false

          return
  )
  app.init()
  return
