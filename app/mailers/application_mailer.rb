class ApplicationMailer < ActionMailer::Base
  default from: "from@example.com"
  layout 'mailer'

  before_filter :set_mailer_host

  def set_mailer_host
    Rails.application.routes.default_url_options[:host] = 'hornet.photostrana.c66.me'
  end
end
