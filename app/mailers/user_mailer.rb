class UserMailer < ApplicationMailer

  default from: 'Фотоконкурс "Карта счастья" <promo@sokolov.ru>'

  def feedback_email(feedback)
    options = {
      :subject => "Новое сообщение через обратную связь",
      :email => 'promo@sokolov.ru',
      :global_merge_vars => [
        {
          name: "LAST_NAME",
          content: "#{feedback.last_name}"
        },
        {
          name: "FIRST_NAME",
          content: "#{feedback.last_name}"
        },
        {
          name: "EMAIL",
          content: "#{feedback.email}"
        },
        {
          name: "MESSAGE",
          content: "#{feedback.message}"
        },
        {
          name: "TS",
          content: "#{feedback.created_at.strftime("HH:MM, dd:mm:YYYY")}"
        }
      ],
      :template => "ps_feedback"
    }
    mandrill_send options
  end

  def uploaded_email(photo)
    @user = User.find(photo.user_id)
    options = {
      :subject => "Ваше фото счастья находится на модерации",
      :email => @user.email,
      :global_merge_vars => [
        {
          name: "FOTOID",
          content: "#{photo.id}"
        }
      ],
      :template => "ps_uploaded_photo"
    }
    mandrill_send options
  end

  def approved_email(photo)
    @user = User.find(photo.user_id)
    options = {
      :subject => "Ваша конкурсная работа прошла модерацию",
      :email => @user.email,
      :global_merge_vars => [
        {
          name: "FOTOID",
          content: "#{photo.id}"
        }
      ],
      :template => "ps_photo_approved"
    }
    mandrill_send options
  end

  def cancelled_email(photo)
    @user = User.find(photo.user_id)
    options = {
      :subject => "К сожалению, ваша конкурсная работа не прошла модерацию",
      :email => @user.email,
      :global_merge_vars => [
        {
          name: "FOTOID",
          content: "#{photo.id}"
        }
      ],
      :template => "ps_photo_cancelled"
    }
    mandrill_send options
  end

  def winner_email(photo)
    @user = User.find(photo.user_id)
    options = {
      :subject => "Ваша конкурсная заявка ПОЛУЧАЕТ ПРИЗ!",
      :email => @user.email,
      :global_merge_vars => [
        {
          name: "FOTOID",
          content: "#{photo.id}"
        }
      ],
      :template => "ps_winner"
    }
    mandrill_send options
  end


  def mandrill_send(opts={})
    message = {
      :from_email => 'promo@sokolov.ru',
      :from_name => 'Фотоконкурс "Карта счастья"',
      :subject => opts[:subject],
      :merge => true,
      :to=>
        [
          {
            "email"=>"#{opts[:email]}",
            "type"=>"to"
          }
        ],
      :global_merge_vars => opts[:global_merge_vars]
    }
    sending = MANDRILL.messages.send_template opts[:template], [], message
    rescue Mandrill::Error => e
      Rails.logger.debug("#{e.class}: #{e.message}")
    raise
  end

end
