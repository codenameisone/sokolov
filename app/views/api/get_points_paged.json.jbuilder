json.points @photos do |point|
  json.id point.id
  json.user_id point.user_id
  json.name point.user.last_name+' '+point.user.first_name
  if user_signed_in?
    if current_user.voted_for? point
      json.voted :true
      json.can_vote :false
    else
      json.voted :false
      json.can_vote :true
    end
  else
    json.voted :false
    json.can_vote :false
  end
  json.votes point.cached_votes_up
  json.path point.image_url(:main)
end
