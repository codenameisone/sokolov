class Photo < ActiveRecord::Base

  belongs_to :user
  belongs_to :region
  belongs_to :week

  has_one :winner

  acts_as_mappable

  paginates_per 16

  mount_uploader :image, ImageUploader
  mount_uploader :tile, TileUploader

  scope :published, -> { where(is_approved: true).where(is_cancelled: false) }

  # Photo.user_last_day(current_user)
  scope :user_last_day, lambda { |user|
    where(:user_id => user.id).where("created_at >= ?", Time.zone.now.beginning_of_day)
  }

  acts_as_votable

  after_create :check_tile

  def check_tile
    Photo.createTile(self)
  end

  def self.createTile(photo)
    photo.tile = File.open(File.join(Rails.root, "data/x.png"))
    photo.save
  end

  def self.createMap
    tiles = []
    error = false
    Region.all.each do |region|
      if Photo.where(:region_id => region.id).where(:is_tile => true).exists?
        tiles.push(Photo.includes(:region).where(:region_id => region.id).where(:is_tile => true).first)
      else
        if Generic.where(:region_id => region.id).exists?
          tiles.push(Generic.where(:region_id => region.id).order('RAND()').first)
        else
          error = true
        end
      end
    end

    if !error

      canvas = ::Magick::Image.read(File.join(Rails.root, "data/canvas.png")).first
      borders = ::Magick::Image.read(File.join(Rails.root, "data/borders.png")).first
      tiles.each do |tile|
        if !tile.nil?
          overlay = ::Magick::Image.read(File.join(Rails.root, "public"+tile.tile_url)).first
          canvas.composite!(overlay, tile.region.tile_left, tile.region.tile_top, Magick::OverCompositeOp)
        end
      end
      canvas.composite!(borders, 0, 0, Magick::OverCompositeOp)

      if File.exists?(File.join(Rails.root, "tmp/m.png"))
        File.delete(File.join(Rails.root, "tmp/m.png"))
      end
      canvas.write(File.join(Rails.root, "tmp/m.png"))

      map = Map.create()
      map.image = File.open(File.join(Rails.root, "tmp/m.png"))
      map.save
    end
  end

  def self.makeFolders
    require 'fileutils'
    Region.all.each do |region|
      x = File.join(Rails.root, "data/photos/", region.alias)
      FileUtils::mkdir_p x
      # Dir.mkdir(File.join(Rails.root, "data/photos/", region.alias))
    end
  end

end
