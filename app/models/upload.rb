class Upload < ActiveRecord::Base

  belongs_to :user

  mount_uploader :image, TempUploader

  validate :file_size

  def file_size
    if image.file.size.to_f/(1000*1000) > 4
      errors.add(:image, "Объем файла превышает 4Мб.")
    end
  end

end
