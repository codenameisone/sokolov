class Generic < ActiveRecord::Base

  belongs_to :region

  acts_as_mappable

  mount_uploader :image, ImageUploader
  mount_uploader :tile, TileUploader

  after_create :check_tile

  def check_tile
    Generic.createTile(self)
  end

  def self.createTile(photo)
    photo.tile = File.open(File.join(Rails.root, "data/x.png"))
    photo.save
  end

  def self.prefill
    Region.all.each do |region|
      region_folder = File.join(Rails.root, "data/photos/"+region.alias)
      if File.directory?(region_folder)
        Dir.entries(region_folder).each do |file|
          if !File.directory? file
            Generic.create(
              region_id: region.id,
              lat: region.lat,
              lng: region.lng,
              image: File.open(region_folder+'/'+file)
            )
          end
        end
      end
    end
  end

  def self.fillBlanks
    Region.all.each do |region|
      if !Generic.where('region_id = ?',region.id).exists?
        Generic.create(
          region_id: region.id,
          lat: region.lat,
          lng: region.lng,
          image: File.open(File.join(Rails.root, "data/blank.png"))
        )
      end
    end
  end

end
