class Week < ActiveRecord::Base

  has_many :photos
  # has_one  :winner
  # has_one  :prize

  has_many :winners
  has_many :prizes

  scope :current, -> do
    today = DateTime.now
    where('starts <= ?', today).where('ends >= ?', today).first
  end

  scope :previous, -> { where('id < ?', current.id ).order('id desc').first }

end
