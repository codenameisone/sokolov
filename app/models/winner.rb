class Winner < ActiveRecord::Base
  belongs_to :user
  belongs_to :week
  belongs_to :photo
  belongs_to :prize

  validates :prize_id, :user_id, :week_id, :photo_id, presence: true

  after_create :send_message

  paginates_per 12

  scope :current, -> { where(week_id: Week.current.id) }

  scope :previous_week, -> { where(week_id: Week.previous.id) }

  scope :former, -> do
    if current.present?
      where.not(id: current.pluck(:id))
    else
      where.not(id: previous_week.pluck(:id))
    end
  end

  def send_message
    UserMailer.winner_email(self.photo).deliver_now
  end
end
