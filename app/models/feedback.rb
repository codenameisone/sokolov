class Feedback < ActiveRecord::Base

  after_create :send_message

  def send_message
    UserMailer.feedback_email(self).deliver_now
  end

end
