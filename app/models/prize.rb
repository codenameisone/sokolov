class Prize < ActiveRecord::Base

  has_many :winners

  paginates_per 20

end
