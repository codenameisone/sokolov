class Map < ActiveRecord::Base

  mount_uploader :image, MapUploader

  paginates_per 5

end
