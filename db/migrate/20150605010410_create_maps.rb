class CreateMaps < ActiveRecord::Migration
  def change
    create_table :maps do |t|
      t.string :image
      t.boolean :use_this, :default => false
      t.timestamps null: false
    end
  end
end
