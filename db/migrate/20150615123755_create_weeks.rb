class CreateWeeks < ActiveRecord::Migration
  def change
    create_table :weeks do |t|
      t.datetime :starts
      t.datetime :ends
      t.integer :number
      t.boolean :has_winner, :default => false
      t.timestamps null: false
    end
  end
end
