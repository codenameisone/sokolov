class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.integer :user_id
      t.integer :region_id
      t.integer :week_id
      t.string :title
      t.string :image
      t.string :tile
      t.float :lat
      t.float :lng
      t.integer :zoom
      t.boolean :is_tile, :default => false
      t.boolean :is_approved, :default => false
      t.boolean :is_cancelled, :default => false
      t.boolean :is_winner, :default => false
      t.boolean :is_fake, :default => false
      t.timestamps null: false
    end
    add_column :photos, :cached_votes_total, :integer, :default => 0
    add_column :photos, :cached_votes_score, :integer, :default => 0
    add_column :photos, :cached_votes_up, :integer, :default => 0
    add_column :photos, :cached_votes_down, :integer, :default => 0
    add_column :photos, :cached_weighted_score, :integer, :default => 0
    add_column :photos, :cached_weighted_total, :integer, :default => 0
    add_column :photos, :cached_weighted_average, :float, :default => 0.0
    add_index  :photos, :cached_votes_total
    add_index  :photos, :cached_votes_score
    add_index  :photos, :cached_votes_up
    add_index  :photos, :cached_votes_down
    add_index  :photos, :cached_weighted_score
    add_index  :photos, :cached_weighted_total
    add_index  :photos, :cached_weighted_average
    add_index :photos, [:is_approved, :lat, :lng]
    add_index :photos, :user_id
    add_index :photos, :region_id
    add_index :photos, :week_id
  end
end
