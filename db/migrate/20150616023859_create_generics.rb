class CreateGenerics < ActiveRecord::Migration
  def change
    create_table :generics do |t|
      t.integer :region_id
      t.string :image
      t.string :tile
      t.float :lat
      t.float :lng
      t.integer :zoom
      t.timestamps null: false
    end
  end
end
