class CreateRegions < ActiveRecord::Migration
  def change
    create_table :regions do |t|
      t.string :title
      t.string :alias
      t.string :alias_path
      t.string :sort_title
      t.integer :sort_order, :default => 10
      t.float :lat
      t.float :lng
      t.integer :zoom
      t.string :tile_image
      t.text :area
      t.integer :tile_top, :default => 0
      t.integer :tile_left, :default => 0
      t.integer :tile_width, :default => 0
      t.integer :tile_height, :default => 0
    end
    add_index :regions, [:id, :sort_order]
    add_index :regions, [:alias]
    add_index :regions, [:alias_path]
  end
end
