class CreateWinners < ActiveRecord::Migration
  def change
    create_table :winners do |t|
      t.integer :week_id
      t.integer :user_id
      t.integer :photo_id
      t.integer :prize_id

      t.timestamps null: false
    end

    add_index :winners, :week_id
  end
end
