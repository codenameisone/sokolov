# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150628114730) do

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "feedbacks", force: :cascade do |t|
    t.string   "first_name", limit: 255
    t.string   "last_name",  limit: 255
    t.string   "email",      limit: 255
    t.text     "message",    limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "generics", force: :cascade do |t|
    t.integer  "region_id",  limit: 4
    t.string   "image",      limit: 255
    t.string   "tile",       limit: 255
    t.float    "lat",        limit: 24
    t.float    "lng",        limit: 24
    t.integer  "zoom",       limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "identities", force: :cascade do |t|
    t.string   "uid",        limit: 255
    t.string   "provider",   limit: 255
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "identities", ["user_id"], name: "index_identities_on_user_id", using: :btree

  create_table "maps", force: :cascade do |t|
    t.string   "image",      limit: 255
    t.boolean  "use_this",   limit: 1,   default: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "photos", force: :cascade do |t|
    t.integer  "user_id",                 limit: 4
    t.integer  "region_id",               limit: 4
    t.integer  "week_id",                 limit: 4
    t.string   "title",                   limit: 255
    t.string   "image",                   limit: 255
    t.string   "tile",                    limit: 255
    t.float    "lat",                     limit: 24
    t.float    "lng",                     limit: 24
    t.integer  "zoom",                    limit: 4
    t.boolean  "is_tile",                 limit: 1,   default: false
    t.boolean  "is_approved",             limit: 1,   default: false
    t.boolean  "is_cancelled",            limit: 1,   default: false
    t.boolean  "is_winner",               limit: 1,   default: false
    t.boolean  "is_fake",                 limit: 1,   default: false
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.integer  "cached_votes_total",      limit: 4,   default: 0
    t.integer  "cached_votes_score",      limit: 4,   default: 0
    t.integer  "cached_votes_up",         limit: 4,   default: 0
    t.integer  "cached_votes_down",       limit: 4,   default: 0
    t.integer  "cached_weighted_score",   limit: 4,   default: 0
    t.integer  "cached_weighted_total",   limit: 4,   default: 0
    t.float    "cached_weighted_average", limit: 24,  default: 0.0
  end

  add_index "photos", ["cached_votes_down"], name: "index_photos_on_cached_votes_down", using: :btree
  add_index "photos", ["cached_votes_score"], name: "index_photos_on_cached_votes_score", using: :btree
  add_index "photos", ["cached_votes_total"], name: "index_photos_on_cached_votes_total", using: :btree
  add_index "photos", ["cached_votes_up"], name: "index_photos_on_cached_votes_up", using: :btree
  add_index "photos", ["cached_weighted_average"], name: "index_photos_on_cached_weighted_average", using: :btree
  add_index "photos", ["cached_weighted_score"], name: "index_photos_on_cached_weighted_score", using: :btree
  add_index "photos", ["cached_weighted_total"], name: "index_photos_on_cached_weighted_total", using: :btree
  add_index "photos", ["is_approved", "lat", "lng"], name: "index_photos_on_is_approved_and_lat_and_lng", using: :btree
  add_index "photos", ["region_id"], name: "index_photos_on_region_id", using: :btree
  add_index "photos", ["user_id"], name: "index_photos_on_user_id", using: :btree
  add_index "photos", ["week_id"], name: "index_photos_on_week_id", using: :btree

  create_table "prizes", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "regions", force: :cascade do |t|
    t.string  "title",       limit: 255
    t.string  "alias",       limit: 255
    t.string  "alias_path",  limit: 255
    t.string  "sort_title",  limit: 255
    t.integer "sort_order",  limit: 4,     default: 10
    t.float   "lat",         limit: 24
    t.float   "lng",         limit: 24
    t.integer "zoom",        limit: 4
    t.string  "tile_image",  limit: 255
    t.text    "area",        limit: 65535
    t.integer "tile_top",    limit: 4,     default: 0
    t.integer "tile_left",   limit: 4,     default: 0
    t.integer "tile_width",  limit: 4,     default: 0
    t.integer "tile_height", limit: 4,     default: 0
  end

  add_index "regions", ["alias"], name: "index_regions_on_alias", using: :btree
  add_index "regions", ["alias_path"], name: "index_regions_on_alias_path", using: :btree
  add_index "regions", ["id", "sort_order"], name: "index_regions_on_id_and_sort_order", using: :btree

  create_table "uploads", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.string   "image",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "",    null: false
    t.string   "encrypted_password",     limit: 255, default: "",    null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "first_name",             limit: 255
    t.string   "middle_name",            limit: 255
    t.string   "last_name",              limit: 255
    t.string   "post_index",             limit: 255
    t.string   "city",                   limit: 255
    t.string   "street",                 limit: 255
    t.string   "street_number",          limit: 255
    t.string   "house_type",             limit: 255
    t.string   "apts_number",            limit: 255
    t.string   "provider",               limit: 255
    t.string   "uid",                    limit: 255
    t.boolean  "is_fake",                limit: 1,   default: false
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "votes", force: :cascade do |t|
    t.integer  "votable_id",   limit: 4
    t.string   "votable_type", limit: 255
    t.integer  "voter_id",     limit: 4
    t.string   "voter_type",   limit: 255
    t.boolean  "vote_flag",    limit: 1
    t.string   "vote_scope",   limit: 255
    t.integer  "vote_weight",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope", using: :btree
  add_index "votes", ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope", using: :btree

  create_table "weeks", force: :cascade do |t|
    t.datetime "starts"
    t.datetime "ends"
    t.integer  "number",     limit: 4
    t.boolean  "has_winner", limit: 1, default: false
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  create_table "winners", force: :cascade do |t|
    t.integer  "week_id",    limit: 4
    t.integer  "user_id",    limit: 4
    t.integer  "photo_id",   limit: 4
    t.integer  "prize_id",   limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "winners", ["week_id"], name: "index_winners_on_week_id", using: :btree

end
