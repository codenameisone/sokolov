require "test_helper"

class WeekTest < ActiveSupport::TestCase
  def week
    @week ||= Week.new
  end

  def test_valid
    assert week.valid?
  end
end
