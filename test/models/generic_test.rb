require "test_helper"

class GenericTest < ActiveSupport::TestCase
  def generic
    @generic ||= Generic.new
  end

  def test_valid
    assert generic.valid?
  end
end
