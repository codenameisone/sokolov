require "test_helper"

class WinnerTest < ActiveSupport::TestCase
  def winner
    @winner ||= Winner.new
  end

  def test_valid
    assert winner.valid?
  end
end
