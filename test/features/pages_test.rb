require "test_helper"

feature "Pages" do
  scenario "Test index" do
    visit root_path
    page.must_have_content "фотоконкурс «Карта Счастья»"
  end

  scenario "Test about" do
    visit root_path
    click_link('about_path')
    page.must_have_content "О конкурсе"
  end

  scenario "Test rules" do
    visit root_path
    click_link('rules_path')
    page.must_have_content "1. ОБЩИЕ ПОЛОЖЕНИЯ"
  end

  scenario "Test winners" do
    visit root_path
    first(:link, 'Победители').click
    page.must_have_content "Победители"
  end

end
