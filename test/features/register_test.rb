require "test_helper"

feature "Register" do
  scenario "Test register pane" do

    Capybara.current_driver = :webkit

    visit root_path
    click_link('register_link')

    save_and_open_page

    within '#register_form' do
      fill_in 'user[last_name]',    with: 'Shkolnikov'
      fill_in 'user[first_name]',    with: 'Alex'
      fill_in 'user[middle_name]',    with: 'A'
      fill_in 'user[post_index]',    with: '2011'
      fill_in 'user[city]',    with: 'Sydney'
      fill_in 'user[street]',    with: 'Bayswater Road'
      fill_in 'user[street_number]',    with: '9-15'
      fill_in 'user[house_type]',    with: ''
      fill_in 'user[apts_number]',    with: '304'
      fill_in 'user[email]',    with: 'alex.shkolnikov@gmail.com'
      fill_in 'user[password]',    with: 'test3333'
      fill_in 'user[password_confirmation]',    with: 'test3333'
      click_button 'Зарегистрироваться'
    end

    page.must_have_content "Выход"
    page.wont_have_content "Регистрация"
  end
end
